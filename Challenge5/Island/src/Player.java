import java.util.Currency;

public class Player {
    private int startxpos = 0, startypos = 0;
    private int xpos;
    private int ypos;

    public String restart(){
        this.xpos = startxpos;
        this.ypos = startypos;

        return currentPos();
    }

    public String pullOver(char arah){
        switch (arah) {
            case 'N':
                ypos = 0;
                break;
            case 'W':
                xpos = 0;
                break;
            case 'E':
                xpos = 9;
                break;
            case 'S':
                ypos = 9;
                break;
            default:
                break;
        }

        return currentPos();

    }

    public String move (char arah, int count){
        switch (arah){
            case 'N':
                if(ypos - count >= 0){
                    ypos -= count;
                }else{
                   ypos = 0;
                }
                break;
            case 'S':
                if(ypos + count <= 9){
                    ypos += count;
                }else{
                    ypos = 9;
                }
                break;
            case 'W':
                if(xpos - count >= 0){
                    xpos -= count;
                }else{
                    xpos = 0;
                }
                break;
            case 'E':
                if(xpos + count <= 9){
                    xpos += count;
                }else{
                    xpos = 9;
                }
                break;
        }

        return currentPos();
    }

    private String currentPos(){
        return String.format("%c%d", ypos + 65, xpos +1);
    }

}
