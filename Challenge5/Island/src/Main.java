import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int choice;
        Player pl = new Player();

        System.out.println(pl.restart());
        System.out.println(pl.pullOver('E'));
        System.out.println(pl.restart());
        System.out.println(pl.pullOver('S'));

        System.out.println(pl.restart());
        System.out.println((pl.move('E', 2)));
        System.out.println((pl.move('S', 7)));
        System.out.println((pl.move('W', 5)));
    }
}
